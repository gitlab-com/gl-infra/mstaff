It is important that we have sufficient additional team member coverage in case there are availability, security, or other customer facing events. This issue aims to identify clear escalation DRIs from various groups and to provide clear references for teams with already established escalations contacts.

This extended coverage period is intended to cover from **__Date and UTC Time__** through **__Date and UTC Time__**.

### Why? 

<!-- 

24/7 coverage by team members in addition to the existing escalation paths for development, infrastructure, security, etc may not be necessary. 
Explain why you feel that it is necessary in this section. For example - 

"During the end of year period many Team Members have important plans with family and friends. 
These all happen at the same time, so there can be days when it would be hard to get support 
for critical issues unless we plan ahead."

-->

This Issue seeks to make sure that our EOC and the IMOC both have clear escalation points from other teams more likely to be involved in solving an incident. This prevents having to randomly call Team Members who may all collectively not be expecting to need to support a critical incident, or worse, relying on a small group of "known" engineers who frequently help in incidents.

### Where?

We will engage in the incident channels, where [#incident-management](https://gitlab.slack.com/archives/CB7P5CJS1) is used as a place where incidents are reported, and [#production](https://gitlab.slack.com/archives/C101F3796) where incidents can be declared.

### Resources
- [Incident runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/incidents/general_incidents.md)
- [Handbook on Incident Management](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/)
- See [this note](https://gitlab.com/gitlab-com/gl-infra/mstaff/-/issues/86#note_679105748) about the "I'm under attack option" in Cloudflare
- [Engaging the Security On-Call](https://about.gitlab.com/handbook/engineering/security/#engaging-the-security-on-call)
- [Engaging the Development On-Call](https://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html#process-outline)

### Coverage

#### Infrastructure EOC

DRI: 

EOCs are scheduled in Pagerduty and are currently scheduled as:
TBD (APAC), TBD(EMEA), and TBD(AMER)

#### Incident Managers

DRI:

Incident Managers are scheduled in Pagerduty and are available from this <pagerduty escalation link>

Special coverage has been coordinated in this [coverage issue](url)

#### Trust & Safety

DRI:  

|**Shift**|**APAC**|**AMER**|**EMEA**|**Notes**|
| ------- | ------ | ------ | ------ | ------- |
|         |        |        |        |         |


#### Security SIRT

DRI: 

|**Shift**|**Team Member**|**Notes**|
| --- | --- | --- |
|     |     |     |
|     |     |     |


#### Security AppSec

DRI:

|**Shift**|**APAC**|**AMER**|**EMEA**|**Notes**|
| ------- | ------ | ------ | ------ | ------- |
|         |        |        |        |         |

#### CMOC

DRI: 

CMOCs are part of the [CMOC Rotation in PD](https://gitlab.pagerduty.com/escalation_policies#PNH1Z1L)

In APAC, the Support Manager on-call is the next escalation point and can will step in. The PD rotation is the SSOT and the above table is for convenience. Days listed are relative to UTC.

|**Shift**|**APAC-1**|**APAC-2**|**EMEA**|**AMER**|**Notes**|
| ------- | ------ | ------ | ------ | ------- | ------- |
|         |        |        |        |         |         |

#### Development follow the sun Manager rotation

DRI: 

Engineering Managers please add yourselves to the slot(s) that you are available. We can have multiple per slot.

|**Shift**|**APAC**|**AMER**|**EMEA**|**Notes**|
| ------- | ------ | ------ | ------ | ------- |
|         |        |        |        |         |

#### Development follow the sun Individual Contributor rotation

DRI:  

We can have multiple per slot.

|**Shift**|**APAC**|**AMER**|**EMEA**|**Notes**|
| ------- | ------ | ------ | ------ | ------- |
|         |        |        |        |         |

#### Development Verify - Pipeline Execution, Pipeline Authoring, Testing

DRI: 

***Escalate to [`#s_verify` on Slack](https://gitlab.slack.com/archives/CPCJ8CCCX) and `@mention` the SME on call**

|**Shift**|**APAC**|**AMER**|**EMEA**|**Notes**|
| ------- | ------ | ------ | ------ | ------- |
|         |        |        |        |         |

#### Development Gitaly Team

DRI: 

|**Shift**|**Team Member**|**Notes**|
| ------- | ------------- | ------- |

#### Code Review, Source Code, Editor, Integrations

DRI:

|**Shift**|**APAC**|**AMER**|**EMEA**|**Notes**|
| ------- | ------ | ------ | ------ | ------- |
|         |        |        |        |         |

#### Fulfillment (`customers.gitlab.com`) 

DRI: 

Probably optional, due to `customers.gitlab.com` normally not having any performance issues

\***Escalate to** `#s_fulfillment_infradev`, `#s_fulfillment_engineering`, `#s_fulfillment_` on Slack
